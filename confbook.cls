\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{confbook}[2019/05/10 Class for Conference Info Booklets]

\newif\ifdraft
\draftfalse
\DeclareOption{draft}{\drafttrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}
\ProcessOptions\relax

\PassOptionsToClass{a5paper,10pt,twoside}{memoir}
\LoadClassWithOptions{memoir}

\RequirePackage{geometry}
% \RequirePackage[margin=3cm]{geometry}
\RequirePackage[mono=false]{libertine}

\RequirePackage[final]{microtype}
\RequirePackage{adforn}
\RequirePackage{enumitem}
% \RequirePackage{booktabs}
\RequirePackage{calc}
\RequirePackage[table]{xcolor}
\RequirePackage[utf8]{inputenc}

\newlist{people}{itemize}{1}
\setlist[people]{
  itemsep=0pt,
  leftmargin=1em,
  % topsep=3pt,
  after={\vspace{6pt}},
  label={\textcolor{gray}{\textbullet}}
}

% \def\arraystretch{1.5}

\newcommand\divider[1][gray]{%
  \noindent\leavevmode%
  \bgroup
  \color{gray}
  \leaders\hrule height 0.8ex depth \dimexpr1.5pt-0.8ex\hfill%
  \kern0pt%
  \egroup
}

\RequirePackage{graphicx}
\RequirePackage{wrapfig}
\setlength\intextsep{0pt}

\def\hackwrapfig{\advance\c@WF@wrappedlines\m@ne}

\def\clearpics{\par{\count@\c@WF@wrappedlines\clear@wrapfig}\par}

\def\clear@wrapfig{{%
\ifnum\count@>\@ne
\noindent\mbox{}\\%
\advance\count@\m@ne
\expandafter\clear@wrapfig
\else
\ifhmode\unskip\unpenalty\fi
\fi}}

\newcommand{\framedpic}[2][0.24\textwidth]{%
  \begin{wrapfigure}{l}{#1}%
  \begingroup%
    \color{gray}%
    \fboxsep=1pt\fboxrule=1pt%
    \fbox{\includegraphics[width=\linewidth-4pt]{#2}}%
  \endgroup%
  \end{wrapfigure}%
}

\newcommand\nicebox[2][gray]{%
  \begingroup%
    \color{#1}%
    \fboxsep=0pt\fboxrule=1pt%
    \fbox{#2}%
  \endgroup%
}

\newenvironment{speaker}[2][0.24\textwidth]{%
  \vspace{.5\onelineskip}
  \newcommand{\bio}[1]{%
    \paragraph{##1}%
  }
  \renewcommand{\abstract}[1]{%
    \hackwrapfig\paragraph{Talk: ##1}%
  }%
  \begin{minipage}{\linewidth}
  \parskip=1.2\onelineskip
  \framedpic[#1]{#2}%
}{%
  \clearpics
  \end{minipage}%
  \par
  \vspace{.5\onelineskip}
}
% \RequirePackage{titlesec}
% \chapterstyle{demo}
% \renewcommand{\beforechapskip}{-1cm}
% \renewcommand{\printchaptername}{}
% \renewcommand{\printchapternum}{}
\setsecnumformat{}
\setsecheadstyle{\makesec}
% \setaftersecskip{\onelineskip}
\setsubsecheadstyle{\makesubsec}
\setbeforesubsecskip{\onelineskip}
\setaftersubsecskip{3pt}

\newlength{\secrulewidth}
\setlength{\secrulewidth}{.6\linewidth}

\newcommand\secclearpage{}
\newcommand\SectionsOnNewPage{\renewcommand\secclearpage{\clearpage}}
\newcommand\SectionsOnSamePage{\renewcommand\secclearpage{}}


\newcommand\makesec[1]{
  \secclearpage
  \begin{minipage}{\linewidth}
  \raggedright
  % \raggedleft
  \LARGE\bfseries #1%
  % \settowidth{\secrulewidth}{#1}
  \vspace{-.7\onelineskip}%\hspace{-5em}
  {\color{gray}\rule{\secrulewidth}{3pt}}
  % \vspace{-\onelineskip}%\hspace{-5em}
  \end{minipage}
}
\newcommand\makesubsec[1]{%
  % \centering%
  % \adforn{20}%
  \bfseries\Large#1%
  % \adforn{22}%
}

\setsecnumdepth{chapter}

\RequirePackage{hyperref}


\makechapterstyle{conf}{%
   \renewcommand*{\printchaptername}{}
   \renewcommand*{\printchapternum}{}%{\chapnumfont \numtoName{\c@chapter}}
   \renewcommand*{\chaptitlefont}{\normalfont\Huge\sffamily\bfseries}
   \renewcommand*{\printchaptertitle}[1]{\chaptitlefont ##1}
   \renewcommand*{\afterchaptertitle}%
                 {\vskip-10pt\relax {\color{gray}\rule{.6\linewidth}{3pt}}
                 \vskip\afterchapskip}
}% end demo

\chapterstyle{conf}

\RequirePackage{multicol}
\SetEnumitemKey{twocol}{
  itemsep=1\itemsep,
  parsep=1\parsep,
  before=\raggedcolumns\begin{multicols}{2},
  after=\end{multicols}}

\setlist{itemsep=0pt}

\newlength{\programmetimewidth}
\newlength{\programmewidth}

\newenvironment{programme}{%
  % \begin{tabular}{r@{\quad}p{.6\linewidth}}
  \setlength{\programmetimewidth}{70pt}%
  \setlength{\programmewidth}{\linewidth}%
  \addtolength{\programmewidth}{-\programmetimewidth}%
  \addtolength{\programmewidth}{-\tabcolsep}%
  \noindent%
  \begin{tabular}{@{}p{\programmetimewidth}@{}p{\programmewidth}@{}}
}{%
  \end{tabular}%
}
\newcommand{\session}[2][]{%
  % \multicolumn{2}{c}{%
  \divider\quad\null%
  &
    % \cellcolor{black!10}%
    \rule{0pt}{2em}
    % \divider\quad%
    \textbf{\sffamily\scshape #2}%
    \quad\divider\if!#1!\else\quad#1\fi%
  % }%
  \\[1ex]
}
\newcommand{\sessionend}{%
  \divider\null&\divider\null\\
}%
\newcommand{\talk}{\@ifstar{\talk@last}{\talk@}}
\newcommand{\talk@}[3]{\rule{0pt}{\dimexpr\baselineskip+.1ex\relax}#1\rlap{\color{gray}\small\hspace{6pt}\raisebox{1pt}{\textbullet}}&#2\\&\emph{#3}\\[.8ex]}
\newcommand{\talk@last}[3]{\rule{0pt}{\dimexpr\baselineskip+.1ex\relax}#1\rlap{\color{gray}\small\hspace{6pt}\raisebox{1pt}{\textbullet}}&#2\\&\emph{#3}\\[2ex]}
\newcommand{\sessionbreak}[3][black!10]{\cellcolor{#1}#2&\rule{0pt}{\dimexpr\baselineskip+.5ex\relax}\cellcolor{#1}#3\\[1ex]}

\makepagestyle{conf}
  \makeoddfoot{conf}{\color{gray}\rightmark}{}{\thepage}
  \makeevenfoot{conf}{\thepage}{}{\color{gray}\leftmark}

\makeoddfoot{plain}{}{}{\thepage}
\makeevenfoot{plain}{\thepage}{}{}

\createmark{section}{both}{nonumber}{}{}
\pagestyle{conf}


\raggedbottom


\setlrmarginsandblock{2.5cm}{*}{1}
\setulmarginsandblock{2cm}{*}{1}
\checkandfixthelayout
\fixpdflayout

% \abnormalparskip{1em}
\setlength{\parindent}{0pt}

\newcommand{\EndOf}[1]{
  \vfill
  \centerline{\adforn{21}\quad \textsc{End of #1}\quad \adforn{49}}
  \vfill
}